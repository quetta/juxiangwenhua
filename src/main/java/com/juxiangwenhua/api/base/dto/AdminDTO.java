package com.juxiangwenhua.api.base.dto;

/**
 * 管理员添加
 * 
 * @author kuisama
 *
 */
public class AdminDTO {

    private String userName;

    private String secretkey;

    /**
     * 返回userName
     * 
     * @return userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置userName
     * 
     * @param userName
     *            userName
     */
    public void setUserName(
            String userName) {
        this.userName = userName;
    }

    /**
     * 返回secretkey
     * 
     * @return secretkey
     */
    public String getSecretkey() {
        return secretkey;
    }

    /**
     * 设置secretkey
     * 
     * @param secretkey
     *            secretkey
     */
    public void setSecretkey(
            String secretkey) {
        this.secretkey = secretkey;
    }

}
