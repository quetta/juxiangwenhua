package com.juxiangwenhua.api.base.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 登录信息
 * 
 * @author kuisama
 *
 */
@ApiModel("登录信息")
public class LoginDTO {

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("密钥")
    private String secretkey;

    /**
     * 返回用户名
     *
     * @return 用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名
     *
     * @param userName
     *            用户名
     */
    public void setUserName(
            String userName) {
        this.userName = userName;
    }

    /**
     * 返回密钥
     *
     * @return 密钥
     */
    public String getSecretkey() {
        return secretkey;
    }

    /**
     * 设置密钥
     *
     * @param secretkey
     *            密钥
     */
    public void setSecretkey(
            String secretkey) {
        this.secretkey = secretkey;
    }
}
