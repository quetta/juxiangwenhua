package com.juxiangwenhua.api.base.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 修改密钥信息
 *
 * @author zhoukui
 *
 */
@ApiModel("修改密钥信息")
public class UpdateSecretkeyDTO {

    @ApiModelProperty("旧密钥")
    private String oldSecretkey;

    @ApiModelProperty("新密钥")
    private String newSecretkey;

    /**
     * 返回旧密钥
     *
     * @return 旧密钥
     */
    public String getOldSecretkey() {
        return oldSecretkey;
    }

    /**
     * 设置旧密钥
     *
     * @param oldSecretkey
     *            旧密钥
     */
    public void setOldSecretkey(
            String oldSecretkey) {
        this.oldSecretkey = oldSecretkey;
    }

    /**
     * 返回新密钥
     *
     * @return 新密钥
     */
    public String getNewSecretkey() {
        return newSecretkey;
    }

    /**
     * 设置新密钥
     *
     * @param newSecretkey
     *            新密钥
     */
    public void setNewSecretkey(
            String newSecretkey) {
        this.newSecretkey = newSecretkey;
    }
}
