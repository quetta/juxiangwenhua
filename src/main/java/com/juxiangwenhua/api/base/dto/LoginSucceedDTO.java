package com.juxiangwenhua.api.base.dto;

import com.juxiangwenhua.api.model.admin.entity.Admin;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 登录成功信息
 *
 * @author zhoukui
 *
 */
@ApiModel("登录成功信息")
public class LoginSucceedDTO {

    @ApiModelProperty("令牌")
    private String token;

    @ApiModelProperty("管理员信息")
    private Admin admin;

    /**
     * 返回令牌
     *
     * @return 令牌
     */
    public String getToken() {
        return token;
    }

    /**
     * 设置令牌
     *
     * @param token
     *            令牌
     */
    public void setToken(
            String token) {
        this.token = token;
    }

    /**
     * 返回管理员信息
     *
     * @return 管理员信息
     */
    public Admin getAdmin() {
        return admin;
    }

    /**
     * 设置管理员信息
     *
     * @param admin
     *            管理员信息
     */
    public void setAdmin(
            Admin admin) {
        this.admin = admin;
    }
}
