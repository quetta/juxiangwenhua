package com.juxiangwenhua.api.base.service.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import com.juxiangwenhua.api.base.service.BaseService;

import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.SearchParamsUtils;
import net.guerlab.web.result.ListObject;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

/**
 * 接口实现
 *
 * @author kuisama
 *
 * @param <M>
 *            通用mapper
 * @param <T>
 *            对象
 */
@Transactional(rollbackFor = Exception.class)
public class BaseServiceImpl<M extends Mapper<T>, T> implements BaseService<T> {

    @Autowired
    protected M mapper;

    @Override
    public T selectOne(
            T entity) {
        return mapper.selectOne(entity);
    }

    @Override
    public T selectById(
            Object id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<T> selectList(
            T entity) {
        return mapper.select(entity);
    }

    @Override
    public List<T> selectList(
            Example example) {
        return mapper.selectByExample(example);
    }

    @Override
    public List<T> selectListAll() {
        return mapper.selectAll();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> selectListAll(
            AbstractSearchParams searchParams) {
        Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];

        Example example = new Example(clazz);

        SearchParamsUtils.setCriteria(searchParams, example);

        return mapper.selectByExample(example);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ListObject<T> selectBySearchParams(
            AbstractSearchParams searchParams) {
        Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];

        Example example = new Example(clazz);

        SearchParamsUtils.setCriteria(searchParams, example);

        Page<T> result = PageMethod.startPage(searchParams.getPageId(), searchParams.getPageSize());
        List<T> list = mapper.selectByExample(example);

        return new ListObject<>(searchParams.getPageSize(), result.getTotal(), () -> list);
    }

    @Override
    public int selectCount(
            T entity) {
        return mapper.selectCount(entity);
    }

    @Override
    public void insert(
            T entity) {
        insertBefore(entity);
        mapper.insert(entity);
        insertAfter(entity);
    }

    @Override
    public void insertSelective(
            T entity) {
        insertBefore(entity);
        mapper.insertSelective(entity);
        insertAfter(entity);
    }

    /**
     * 添加前
     *
     * @param entity
     *            实体
     */
    protected void insertBefore(
            T entity) {

    }

    /**
     * 添加后
     *
     * @param entity
     *            实体
     */
    protected void insertAfter(
            T entity) {

    }

    @Override
    public void updateById(
            T entity) {
        updateBefore(entity);
        mapper.updateByPrimaryKey(entity);
        updateAfter(entity);
    }

    @Override
    public void updateSelectiveById(
            T entity) {
        updateBefore(entity);
        mapper.updateByPrimaryKeySelective(entity);
        updateAfter(entity);
    }

    @Override
    public void updateByExample(
            T entity,
            Example example) {
        updateBefore(entity);
        mapper.updateByExample(entity, example);
        updateAfter(entity);
    }

    @Override
    public void updateByExampleSelective(
            T entity,
            Example example) {
        updateBefore(entity);
        mapper.updateByExampleSelective(entity, example);
        updateAfter(entity);
    }

    /**
     * 更新前
     *
     * @param entity
     *            实体
     */
    protected void updateBefore(
            T entity) {

    }

    /**
     * 更新后
     *
     * @param entity
     *            实体
     */
    protected void updateAfter(
            T entity) {

    }

    @Override
    public void delete(
            T entity) {
        deleteBefore(entity);
        mapper.delete(entity);
        deleteAfter(entity);
    }

    /**
     * 删除前
     *
     * @param entity
     *            实体
     */
    protected void deleteBefore(
            T entity) {

    }

    /**
     * 删除后
     *
     * @param entity
     *            实体
     */
    protected void deleteAfter(
            T entity) {

    }

    @Override
    public void deleteById(
            Object id) {
        deleteByIdBefore(id);
        mapper.deleteByPrimaryKey(id);
        deleteByIdAfter(id);
    }

    /**
     * 删除前
     *
     * @param id
     *            id
     */
    protected void deleteByIdBefore(
            Object id) {

    }

    /**
     * 删除后
     *
     * @param id
     *            id
     */
    protected void deleteByIdAfter(
            Object id) {

    }

}
