package com.juxiangwenhua.api.base.service;

import java.util.List;

import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.web.result.ListObject;
import tk.mybatis.mapper.entity.Example;

/**
 * 基础接口类
 *
 * @author kuisama
 *
 * @param <T>
 *            对象
 */
public interface BaseService<T> {

    /**
     * 查询
     *
     * @param entity
     *            实体
     * @return 实体
     */
    T selectOne(
            T entity);

    /**
     * 通过Id查询
     *
     * @param id
     *            主键id
     * @return 实体
     */
    T selectById(
            Object id);

    /**
     * 查询列表
     *
     * @param entity
     *            实体
     * @return 实体列表
     */
    List<T> selectList(
            T entity);

    /**
     * 查询列表
     *
     * @param example
     *            Example查询对象
     * @return 实体列表
     */
    List<T> selectList(
            Example example);

    /**
     * 获取所有对象
     *
     * @return 实体列表
     */
    List<T> selectListAll();

    /**
     * 获取所有对象
     *
     * @param searchParams
     *            搜索参数对象
     * @return 实体列表
     */
    List<T> selectListAll(
            AbstractSearchParams searchParams);

    /**
     * 查询列表
     *
     * @param searchParams
     *            搜索参数对象
     * @return 实体列表
     */
    ListObject<T> selectBySearchParams(
            AbstractSearchParams searchParams);

    /**
     * 查询总记录数
     *
     * @param entity
     *            实体
     * @return 实体总数
     */
    int selectCount(
            T entity);

    /**
     * 添加
     *
     * @param entity
     *            实体
     */
    void insert(
            T entity);

    /**
     * 插入 不插入null字段
     *
     * @param entity
     *            实体
     */
    void insertSelective(
            T entity);

    /**
     * 根据id更新
     *
     * @param entity
     *            实体
     */
    void updateById(
            T entity);

    /**
     * 不update null
     *
     * @param entity
     *            实体
     */
    void updateSelectiveById(
            T entity);

    /**
     * 根据条件更新
     *
     * @param entity
     *            实体
     * @param example
     *            条件
     */
    void updateByExample(
            T entity,
            Example example);

    /**
     * 根据条件更新 不update null
     *
     * @param entity
     *            实体
     * @param example
     *            条件
     */
    void updateByExampleSelective(
            T entity,
            Example example);

    /**
     * 删除
     *
     * @param entity
     *            实体
     */
    void delete(
            T entity);

    /**
     * 根据Id删除
     *
     * @param id
     *            主键值
     */
    void deleteById(
            Object id);

}
