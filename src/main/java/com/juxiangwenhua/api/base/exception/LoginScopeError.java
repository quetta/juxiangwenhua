package com.juxiangwenhua.api.base.exception;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 权限不足
 *
 * @author zhoukui
 *
 */
public class LoginScopeError extends ApplicationException {

    private static final long serialVersionUID = 1L;

    /**
     * 权限不足
     */
    public LoginScopeError() {
        super("权限不足", 401);
    }
}
