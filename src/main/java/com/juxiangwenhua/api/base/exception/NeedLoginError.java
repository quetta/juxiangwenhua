package com.juxiangwenhua.api.base.exception;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 需要登录
 *
 * @author zhoukui
 *
 */
public class NeedLoginError extends ApplicationException {

    private static final long serialVersionUID = 1L;

    /**
     * 需要登录
     */
    public NeedLoginError() {
        super("请登录", 401);
    }
}
