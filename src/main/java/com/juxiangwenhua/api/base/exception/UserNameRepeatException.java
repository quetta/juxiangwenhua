package com.juxiangwenhua.api.base.exception;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 用户名重复
 *
 * @author zhoukui
 *
 */
public class UserNameRepeatException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public static final String MESSAGE_KEY = "用户名重复";

    /**
     * 用户名重复
     */
    public UserNameRepeatException() {
        super(MESSAGE_KEY);
    }
}
