package com.juxiangwenhua.api.base.exception;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 管理员无效
 *
 * @author zhoukui
 *
 */
public class AdminInvalidException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public static final String MESSAGE_KEY = "管理员无效";

    public AdminInvalidException() {
        super(MESSAGE_KEY);
    }

}
