package com.juxiangwenhua.api.base.exception;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 登陆失败
 *
 * @author zhoukui
 *
 */
public class LoginFailError extends ApplicationException {

    private static final long serialVersionUID = 1L;

    /**
     * 登陆失败
     *
     * @param message
     *            错误消息
     * @param errorCode
     *            错误码
     */
    public LoginFailError(String message, int errorCode) {
        super(message, errorCode);
    }
}
