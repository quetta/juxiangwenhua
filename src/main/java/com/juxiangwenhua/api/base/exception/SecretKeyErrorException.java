package com.juxiangwenhua.api.base.exception;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 密码错误异常
 *
 *
 */
public class SecretKeyErrorException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public static final String MESSAGE_KEY = "密码错误异常";

    /**
     * 密码错误异常
     */
    public SecretKeyErrorException() {
        super(MESSAGE_KEY);
    }
}
