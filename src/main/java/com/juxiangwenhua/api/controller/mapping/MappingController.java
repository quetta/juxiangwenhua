package com.juxiangwenhua.api.controller.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.controller.admin.dto.MappingDTO;
import com.juxiangwenhua.api.model.mapping.entity.VPMapping;
import com.juxiangwenhua.api.model.mapping.service.VPMappingService;
import com.juxiangwenhua.api.model.vedio.entity.Vedio;
import com.juxiangwenhua.api.model.vedio.service.VedioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "图片-视频关系")
@RestController
@RequestMapping("/mapping")
public class MappingController {

    @Autowired
    private VPMappingService mappingService;

    @Autowired
    private VedioService vedioService;

    @ApiOperation("通过图片id获取对应视频")
    @GetMapping("/get")
    public MappingDTO get(
            Long pictureId) {
        VPMapping mapping = mappingService.findVedio(pictureId);
        Vedio vedio = vedioService.selectById(mapping.getVedioId());
        MappingDTO dto = new MappingDTO();
        dto.setMapping(mapping);
        dto.setVedio(vedio);
        return dto;
    }

}
