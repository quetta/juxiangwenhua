package com.juxiangwenhua.api.controller.ser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.model.ser.entity.SecCategory;
import com.juxiangwenhua.api.model.ser.entity.ServiceIntroduce;
import com.juxiangwenhua.api.model.ser.entity.TopCategory;
import com.juxiangwenhua.api.model.ser.service.SecCategoryService;
import com.juxiangwenhua.api.model.ser.service.ServiceIntroduceService;
import com.juxiangwenhua.api.model.ser.service.TopCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 服务分类
 * 
 * @author kuisama
 *
 */
@Api(tags = "服务分类")
@RestController
@RequestMapping("/ser")
public class ServiceIntroduceController {

    @Autowired
    private TopCategoryService topCategoryservice;

    @Autowired
    private SecCategoryService secCategoryservice;

    @Autowired
    private ServiceIntroduceService service;

    @ApiOperation("获取所有一级分类")
    @GetMapping
    public List<TopCategory> findAllTopCategory() {
        return topCategoryservice.selectListAll();
    }

    @ApiOperation("通过一级分类id查询所有二级分类")
    @GetMapping("/top/{topCategoryId}")
    public List<SecCategory> findSecCategoryByTopCategoryId(
            @ApiParam(value = "一级分类id", required = true) @PathVariable Long topCategoryId) {
        SecCategory entity = new SecCategory();
        entity.setTopCategoryId(topCategoryId);
        return secCategoryservice.selectList(entity);

    }

    @ApiOperation("通过二级分类id查询对应的软文")
    @GetMapping("/sec/{secCategoryId}")
    public ServiceIntroduce findSecCategoryBySecCategoryId(
            @ApiParam(value = "二级分类id", required = true) @PathVariable Long secCategoryId) {
        ServiceIntroduce entity = new ServiceIntroduce();
        entity.setSecCategoryId(secCategoryId);
        return service.selectOne(entity);
    }
}
