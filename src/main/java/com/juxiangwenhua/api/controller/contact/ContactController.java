package com.juxiangwenhua.api.controller.contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.model.contact.entity.Contact;
import com.juxiangwenhua.api.model.contact.service.ContactService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "contact控制器")
@RestController
@RequestMapping("/contact")
public class ContactController {

    @Autowired
    private ContactService service;

    @ApiOperation("新增")
    @PostMapping
    public Contact save(
            @RequestBody Contact contact) {

        service.insert(contact);
        return contact;
    }

}
