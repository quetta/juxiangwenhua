package com.juxiangwenhua.api.controller.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.model.article.entity.Article;
import com.juxiangwenhua.api.model.article.searchparams.ArticleSearchParams;
import com.juxiangwenhua.api.model.article.service.ArticleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "文章控制器")
@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    private ArticleService service;

    @ApiOperation("查询详情")
    @GetMapping("/{articleId}")
    public Article findOne(
            @ApiParam(value = "文章ID", required = true) @PathVariable Long articleId) {
        return service.selectById(articleId);
    }

    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Article> findList(
            ArticleSearchParams searchParams) {
        return service.selectBySearchParams(searchParams);
    }

}
