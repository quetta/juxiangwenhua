package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.article.entity.Article;
import com.juxiangwenhua.api.model.article.searchparams.ArticleSearchParams;
import com.juxiangwenhua.api.model.article.service.ArticleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "文章控制器")
@RestController
@RequestMapping("admin/article")
public class AdminArticleController {

    @Autowired
    private ArticleService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{articleId}")
    public Article findOne(
            @ApiParam(value = "文章ID", required = true) @PathVariable Long articleId) {
        return service.selectById(articleId);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Article> findList(
            ArticleSearchParams searchParams) {

        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("新增")
    @PostMapping
    public Article save(
            @RequestBody Article article) {
        service.insert(article);
        return article;
    }

    
    @ApiOperation("修改")
    @PutMapping("{articleId}")
    public Article update(
            @ApiParam(value = "文章ID", required = true) @PathVariable Long articleId,
            @RequestBody Article article) {

        Article art = service.selectById(articleId);
        if (art == null) {
            throw new ApplicationException("对象无效");
        }

        article.setArticleId(articleId);

        service.updateById(article);

        return article;
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{articleId}")
    public void delete(
            @ApiParam(value = "文章ID", required = true) @PathVariable Long articleId) {
        Article article = service.selectById(articleId);
        if (article == null) {
            throw new ApplicationException("对象无效");
        }
        service.deleteById(articleId);
    }
}
