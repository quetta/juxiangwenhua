package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.contact.entity.Contact;
import com.juxiangwenhua.api.model.contact.searchparams.ContactSearchParams;
import com.juxiangwenhua.api.model.contact.service.ContactService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "联系我们控制器")
@RestController
@RequestMapping("admin/contact")
public class AdminContactController {

    @Autowired
    private ContactService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{pictureId}")
    public Contact findOne(
            @ApiParam(value = "文件ID", required = true) @PathVariable Long contactId) {
        return service.selectById(contactId);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Contact> findList(
            ContactSearchParams searchParams) {

        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{contactId}")
    public void delete(
            @ApiParam(value = "联系我们ID", required = true) @PathVariable Long contactId) {
        Contact contact = service.selectById(contactId);
        if (contact == null) {
            throw new ApplicationException("对象无效");
        }
        service.deleteById(contactId);
    }
}
