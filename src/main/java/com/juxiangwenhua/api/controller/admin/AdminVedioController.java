package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.vedio.entity.Vedio;
import com.juxiangwenhua.api.model.vedio.searchparams.VedioSearchParams;
import com.juxiangwenhua.api.model.vedio.service.VedioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "视频控制器")
@RestController
@RequestMapping("admin/vedio")
public class AdminVedioController {

    @Autowired
    private VedioService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{vedioId}")
    public Vedio findOne(
            @ApiParam(value = "文件ID", required = true) @PathVariable Long vedioId) {
        return findOne0(vedioId);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Vedio> findList(
            VedioSearchParams searchParams) {
        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("新增")
    @PostMapping
    public Vedio save(
            @RequestBody Vedio vedio) {
        service.insert(vedio);
        return vedio;
    }

    
    @ApiOperation("修改")
    @PutMapping("{vedioId}")
    public Vedio update(
            @ApiParam(value = "视频ID", required = true) @PathVariable Long vedioId,
            @RequestBody Vedio vedio) {

        findOne0(vedioId);
        vedio.setVedioId(vedioId);
        service.updateSelectiveById(vedio);
        return vedio;
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{vedioId}")
    public void delete(
            @ApiParam(value = "视频ID", required = true) @PathVariable Long vedioId) {
        findOne0(vedioId);
        service.deleteById(vedioId);
    }

    private Vedio findOne0(
            Long vedioId) {
        Vedio vedio = service.selectById(vedioId);
        if (vedio == null) {
            throw new ApplicationException("对象无效");
        }
        return vedio;
    }
}
