package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.base.dto.AdminDTO;
import com.juxiangwenhua.api.base.dto.LoginDTO;
import com.juxiangwenhua.api.base.dto.LoginSucceedDTO;
import com.juxiangwenhua.api.base.dto.UpdateSecretkeyDTO;
import com.juxiangwenhua.api.base.exception.AdminInvalidException;
import com.juxiangwenhua.api.base.exception.SecretKeyErrorException;
import com.juxiangwenhua.api.commons.aspect.Log;
import com.juxiangwenhua.api.commons.filter.AdminLoginFilter;
import com.juxiangwenhua.api.commons.jwt.AdminJwtHelper;
import com.juxiangwenhua.api.commons.jwt.AdminSecretkeyHelper;
import com.juxiangwenhua.api.commons.jwt.JwtConfig;
import com.juxiangwenhua.api.model.admin.entity.Admin;
import com.juxiangwenhua.api.model.admin.searchparams.AdminSearchParams;
import com.juxiangwenhua.api.model.admin.service.AdminService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

@Api(tags = "管理员")
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private AdminService service;

    @Autowired
    private AdminJwtHelper jwtHelper;

    @ApiOperation("查询管理员详情")
    @GetMapping("/{adminId}")
    public Admin findOne(
            @ApiParam(value = "管理员ID", required = true) @PathVariable Long adminId) {
        return findOne0(adminId);
    }

    @Log
    @ApiOperation("查询管理员列表")
    @GetMapping
    public ListObject<Admin> findList(
            AdminSearchParams searchParams) {
        return service.selectBySearchParams(searchParams);
    }

    @ApiOperation("添加")
    @PostMapping
    public Admin save(
            @RequestBody AdminDTO dto) {
        Admin entity = new Admin();
        entity.setUserName(dto.getUserName());
        service.save(entity, dto.getSecretkey());
        return entity;
    }

    @ApiOperation("登陆")
    @PostMapping("/login")
    public LoginSucceedDTO login(
            @ApiParam(value = "登陆请求数据") @RequestBody LoginDTO dto) {
        Admin admin = service.login(dto);

        return loginSuccess(admin);
    }

    @ApiOperation("修改密码")
    @PostMapping("/updateSecretkey")
    public void updateSecretkey(
            @ApiParam(hidden = true) @RequestAttribute(AdminLoginFilter.ID) Long adminId,
            @RequestBody UpdateSecretkeyDTO dto) {
        Admin admin = findOne0(adminId);

        if (!AdminSecretkeyHelper.isSecretkeyValid(admin, dto.getOldSecretkey())) {
            throw new SecretKeyErrorException();
        }
        service.updateSecretkey(admin, dto.getNewSecretkey());
    }

    @ApiOperation("删除管理员")
    @DeleteMapping("/{adminId}")
    public void delete(
            @ApiParam(value = "管理员ID", required = true) @PathVariable Long adminId) {
        Admin admin = findOne0(adminId);
        if (admin == null) {
            throw new ApplicationException("管理员无效");
        }

        service.deleteById(adminId);
    }

    private LoginSucceedDTO loginSuccess(
            Admin admin) {

        LoginSucceedDTO loginSucceedDTO = new LoginSucceedDTO();
        loginSucceedDTO.setAdmin(admin);
        loginSucceedDTO.setToken(jwtHelper.create(admin, jwtConfig.getTtlMillis()));
        return loginSucceedDTO;
    }

    private Admin findOne0(
            Long adminId) {
        Admin entity = new Admin();
        entity.setAdminId(adminId);
        Admin admin = service.selectOne(entity);

        if (admin == null) {
            throw new AdminInvalidException();
        }

        return admin;
    }

}
