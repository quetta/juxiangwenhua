package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.ser.entity.TopCategory;
import com.juxiangwenhua.api.model.ser.searchparams.TopCategorySearchParams;
import com.juxiangwenhua.api.model.ser.service.TopCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 * 一级分类
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@Api(tags = "一级分类")
@RestController
@RequestMapping("/admin/top/category")
public class AdminTopCategoryController {

    @Autowired
    private TopCategoryService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{topCategoryId}")
    public TopCategory findOne(
            @ApiParam(value = "一级分类ID", required = true) @PathVariable Long topCategoryId) {
        return findOne0(topCategoryId);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<TopCategory> findList(
            TopCategorySearchParams searchParams) {
        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("新增")
    @PostMapping
    public TopCategory save(
            @RequestBody TopCategory topCategory) {
        service.insert(topCategory);
        return topCategory;
    }

    
    @ApiOperation("修改")
    @PutMapping("{topCategoryId}")
    public TopCategory update(
            @ApiParam(value = "一级分类ID", required = true) @PathVariable Long topCategoryId,
            @RequestBody TopCategory topCategory) {

        findOne0(topCategoryId);
        topCategory.setTopCategoryId(topCategoryId);
        service.updateSelectiveById(topCategory);
        return topCategory;
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{topCategoryId}")
    public void delete(
            @ApiParam(value = "一级分类ID", required = true) @PathVariable Long topCategoryId) {
        findOne0(topCategoryId);
        service.deleteById(topCategoryId);
    }

    private TopCategory findOne0(
            Long topCategoryId) {
        TopCategory topCategory = service.selectById(topCategoryId);
        if (topCategory == null) {
            throw new ApplicationException("对象无效");
        }
        return topCategory;
    }
}
