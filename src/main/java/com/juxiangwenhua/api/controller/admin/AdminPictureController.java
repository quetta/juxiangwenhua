package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.picture.entity.Picture;
import com.juxiangwenhua.api.model.picture.searchparams.PictureSearchParams;
import com.juxiangwenhua.api.model.picture.service.PictureService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "图片控制器")
@RestController
@RequestMapping("admin/picture")
public class AdminPictureController {

    @Autowired
    private PictureService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{pictureId}")
    public Picture findOne(
            @ApiParam(value = "文件ID", required = true) @PathVariable Long pictureId) {
        return findOne0(pictureId);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Picture> findList(
            PictureSearchParams searchParams) {

        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("新增")
    @PostMapping
    public Picture save(
            @RequestBody Picture picture) {

        service.insert(picture);
        return picture;
    }

    
    @ApiOperation("修改")
    @PostMapping("{pictureId}")
    public Picture update(
            @ApiParam(value = "图片ID", required = true) @PathVariable Long pictureId,
            @RequestBody Picture picture) {
        findOne0(pictureId);
        picture.setPictureId(pictureId);

        service.updateSelectiveById(picture);

        return picture;
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{pictureId}")
    public void delete(
            @ApiParam(value = "图片ID", required = true) @PathVariable Long pictureId) {
        findOne0(pictureId);
        service.deleteById(pictureId);
    }

    private Picture findOne0(
            Long pictureId) {
        Picture picture = service.selectById(pictureId);
        if (picture == null) {
            throw new ApplicationException("对象无效");
        }
        return picture;
    }
}
