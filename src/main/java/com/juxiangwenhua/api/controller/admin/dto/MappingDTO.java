package com.juxiangwenhua.api.controller.admin.dto;

import com.juxiangwenhua.api.model.mapping.entity.VPMapping;
import com.juxiangwenhua.api.model.vedio.entity.Vedio;

/**
 * 
 * @author kuisama
 *
 */
public class MappingDTO {

    private VPMapping mapping;

    private Vedio vedio;

    /**
     * 返回mapping
     * 
     * @return mapping
     */
    public VPMapping getMapping() {
        return mapping;
    }

    /**
     * 设置mapping
     * 
     * @param mapping
     *            mapping
     */
    public void setMapping(
            VPMapping mapping) {
        this.mapping = mapping;
    }

    /**
     * 返回vedio
     * 
     * @return vedio
     */
    public Vedio getVedio() {
        return vedio;
    }

    /**
     * 设置vedio
     * 
     * @param vedio
     *            vedio
     */
    public void setVedio(
            Vedio vedio) {
        this.vedio = vedio;
    }

}
