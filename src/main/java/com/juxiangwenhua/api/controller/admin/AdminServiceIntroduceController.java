package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.ser.entity.ServiceIntroduce;
import com.juxiangwenhua.api.model.ser.searchparams.ServiceIntroduceSearchParams;
import com.juxiangwenhua.api.model.ser.service.ServiceIntroduceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "服务介绍")
@RestController
@RequestMapping("admin/service/introduce")
public class AdminServiceIntroduceController {

    @Autowired
    private ServiceIntroduceService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{id}")
    public ServiceIntroduce findOne(
            @ApiParam(value = "id", required = true) @PathVariable Long id) {
        return service.selectById(id);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<ServiceIntroduce> findList(
            ServiceIntroduceSearchParams searchParams) {

        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("新增")
    @PostMapping
    public ServiceIntroduce save(
            @RequestBody ServiceIntroduce serviceIntroduce) {
        ServiceIntroduce entity = new ServiceIntroduce();
        entity.setSecCategoryId(serviceIntroduce.getSecCategoryId());
        if (service.selectCount(entity) > 0) {
            throw new ApplicationException("该分类下的软文已存在");
        }
        service.insert(serviceIntroduce);
        return serviceIntroduce;
    }

    
    @ApiOperation("修改")
    @PutMapping("{id}")
    public ServiceIntroduce update(
            @ApiParam(value = "id", required = true) @PathVariable Long id,
            @RequestBody ServiceIntroduce serviceIntroduce) {

        ServiceIntroduce art = service.selectById(id);
        if (art == null) {
            throw new ApplicationException("对象无效");
        }

        serviceIntroduce.setId(id);

        service.updateById(serviceIntroduce);

        return serviceIntroduce;
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{id}")
    public void delete(
            @ApiParam(value = "id", required = true) @PathVariable Long id) {
        ServiceIntroduce serviceIntroduce = service.selectById(id);
        if (serviceIntroduce == null) {
            throw new ApplicationException("对象无效");
        }
        service.deleteById(id);
    }
}
