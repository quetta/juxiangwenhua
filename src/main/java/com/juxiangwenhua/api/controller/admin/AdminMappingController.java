package com.juxiangwenhua.api.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.mapping.entity.VPMapping;
import com.juxiangwenhua.api.model.mapping.searchparams.VPMappingSearchParams;
import com.juxiangwenhua.api.model.mapping.service.VPMappingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.web.result.ListObject;

@Api(tags = "图片-视频关系")
@RestController
@RequestMapping("admin/mapping")
public class AdminMappingController {

    @Autowired
    private VPMappingService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{pictureId}")
    public VPMapping findOne(
            @ApiParam(value = "图片id", required = true) @PathVariable Long pictureId) {
        return service.findOne(pictureId);
    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<VPMapping> findList(
            VPMappingSearchParams searchParams) {

        return new ListObject<>(searchParams.getPageSize(), service.findCount(searchParams),
                () -> service.findList(searchParams));

    }

    
    @ApiOperation("保存关系")
    @PostMapping("/save")
    public VPMapping save(
            @RequestBody VPMapping mapping) {

        return service.save(mapping);
    }

    
    @ApiOperation("删除关系")
    @DeleteMapping("/{pictureId}/{vedioId}")
    public void deleteByStudentId(
            @ApiParam(value = "图片id", required = true) @PathVariable Long pictureId,
            @ApiParam(value = "视频id", required = true) @PathVariable Long vedioId) {

        VPMapping mapping = new VPMapping();
        mapping.setPictureId(pictureId);
        mapping.setVedioId(vedioId);
        service.delete(mapping);
    }
}
