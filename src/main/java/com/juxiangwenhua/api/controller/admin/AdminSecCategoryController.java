package com.juxiangwenhua.api.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.juxiangwenhua.api.model.ser.entity.SecCategory;
import com.juxiangwenhua.api.model.ser.searchparams.SecCategorySearchParams;
import com.juxiangwenhua.api.model.ser.service.SecCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.web.result.ListObject;

/**
 * 二级分类
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@Api(tags = "二级分类")
@RestController
@RequestMapping("/admin/sec/category")
public class AdminSecCategoryController {

    @Autowired
    private SecCategoryService service;

    
    @ApiOperation("查询详情")
    @GetMapping("/{secCategoryId}")
    public SecCategory findOne(
            @ApiParam(value = "二级分类ID", required = true) @PathVariable Long secCategoryId) {
        return findOne0(secCategoryId);
    }

    
    @ApiOperation("通过一级分类id查询该分类下的二级分类")
    @GetMapping("/top/{topCategoryId}")
    public List<SecCategory> findSecCategoryByTopCategoryId(
            @ApiParam(value = "一级分类id", required = true) @PathVariable Long topCategoryId) {
        SecCategory entity = new SecCategory();
        entity.setTopCategoryId(topCategoryId);
        return service.selectList(entity);

    }

    
    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<SecCategory> findList(
            SecCategorySearchParams searchParams) {
        return service.selectBySearchParams(searchParams);
    }

    
    @ApiOperation("新增")
    @PostMapping
    public SecCategory save(
            @RequestBody SecCategory secCategory) {
        service.insert(secCategory);
        return secCategory;
    }

    
    @ApiOperation("修改")
    @PutMapping("{secCategoryId}")
    public SecCategory update(
            @ApiParam(value = "二级分类ID", required = true) @PathVariable Long secCategoryId,
            @RequestBody SecCategory secCategory) {

        findOne0(secCategoryId);
        secCategory.setSecCategoryId(secCategoryId);
        service.updateSelectiveById(secCategory);
        return secCategory;
    }

    
    @ApiOperation("删除")
    @DeleteMapping("{topCategoryId}")
    public void delete(
            @ApiParam(value = "一级分类ID", required = true) @PathVariable Long topCategoryId) {
        findOne0(topCategoryId);
        service.deleteById(topCategoryId);
    }

    private SecCategory findOne0(
            Long secCategoryId) {
        SecCategory secCategory = service.selectById(secCategoryId);
        if (secCategory == null) {
            throw new ApplicationException("对象无效");
        }
        return secCategory;
    }
}
