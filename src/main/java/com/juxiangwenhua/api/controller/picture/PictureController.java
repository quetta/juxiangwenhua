package com.juxiangwenhua.api.controller.picture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.model.picture.entity.Picture;
import com.juxiangwenhua.api.model.picture.searchparams.PictureSearchParams;
import com.juxiangwenhua.api.model.picture.service.PictureService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "图片控制器")
@RestController
@RequestMapping("/picture")
public class PictureController {

    @Autowired
    private PictureService service;

    @ApiOperation("查询详情")
    @GetMapping("/{pictureId}")
    public Picture findOne(
            @ApiParam(value = "文件ID", required = true) @PathVariable Long pictureId) {
        return service.selectById(pictureId);
    }

    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Picture> findList(
            PictureSearchParams searchParams) {

        return service.selectBySearchParams(searchParams);
    }
}
