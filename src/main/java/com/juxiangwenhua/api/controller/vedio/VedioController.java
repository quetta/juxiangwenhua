package com.juxiangwenhua.api.controller.vedio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxiangwenhua.api.model.vedio.entity.Vedio;
import com.juxiangwenhua.api.model.vedio.searchparams.VedioSearchParams;
import com.juxiangwenhua.api.model.vedio.service.VedioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.web.result.ListObject;

/**
 *
 * @author kuisama
 *
 */
@Api(tags = "视频控制器")
@RestController
@RequestMapping("/vedio")
public class VedioController {

    @Autowired
    private VedioService service;

    @ApiOperation("查询详情")
    @GetMapping("/{vedioId}")
    public Vedio findOne(
            @ApiParam(value = "文件ID", required = true) @PathVariable Long vedioId) {
        return service.selectById(vedioId);
    }

    @ApiOperation("查询列表")
    @GetMapping
    public ListObject<Vedio> findList(
            VedioSearchParams searchParams) {
        return service.selectBySearchParams(searchParams);
    }
}
