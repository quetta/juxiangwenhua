package com.juxiangwenhua.api.controller.upload;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.guerlab.spring.upload.entity.FileInfo;
import net.guerlab.spring.upload.helper.UploadFileHelper;

/**
 * 文件上传
 * 
 * @author kuisama
 *
 */
@Api(tags = "文件上传")
@RestController
@RequestMapping
public class UploadFileController {

    private static final String DEFAULT_PATH = "/";

    @ApiOperation("文件上传")
    @PostMapping({
            "/upload", "/upload/{path}"
    })
    public FileInfo upload(
            @ApiParam(value = "上传路径", required = false) @PathVariable(required = false) String path,
            @ApiParam(value = "待上传文件", required = true) @RequestParam MultipartFile file) {
        return UploadFileHelper.upload(file, path == null ? DEFAULT_PATH : path);

    }

    @ApiOperation("多文件列表上传")
    @PostMapping({
            "/multipleUpload", "/multipleUpload/{path}"
    })
    public List<FileInfo> uploadFileList(
            @ApiParam(value = "上传路径", required = false) @PathVariable(required = false) String path,
            @ApiParam(value = "待上传文件", required = true) @RequestParam List<MultipartFile> fileList) {
        return UploadFileHelper.multiUpload(fileList, path == null ? DEFAULT_PATH : path);
    }
}
