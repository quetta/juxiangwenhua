package com.juxiangwenhua.api.commons.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAspect {

    private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Pointcut("@annotation(com.juxiangwenhua.api.commons.aspect.Log)")
    private void cut() {
        logger.info("service 日志记录方式启动!");
    }

    @Before("cut()")
    public void doBefore(
            JoinPoint joinPoint) throws Exception {

        Object[] methodParams = joinPoint.getArgs();
        logger.info("ip:" + methodParams.getClass().getName());

    }

}
