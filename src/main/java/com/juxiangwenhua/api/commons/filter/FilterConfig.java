package com.juxiangwenhua.api.commons.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 拦截器配置
 *
 * @author zhoukui
 */
@Configuration
public class FilterConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private AdminLoginFilter adminLoginFilter;

    @Override
    public void addInterceptors(
            InterceptorRegistry registry) {

        registry.addInterceptor(adminLoginFilter).addPathPatterns("/admin/**").excludePathPatterns("/admin/login");
        super.addInterceptors(registry);
    }
}
