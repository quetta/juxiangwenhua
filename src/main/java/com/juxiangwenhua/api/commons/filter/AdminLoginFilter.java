package com.juxiangwenhua.api.commons.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.juxiangwenhua.api.base.exception.NeedLoginError;
import com.juxiangwenhua.api.commons.jwt.AdminJwtHelper;
import com.juxiangwenhua.api.commons.jwt.AdminJwtHelper.Response;
import com.juxiangwenhua.api.model.admin.entity.Admin;

/**
 * 管理员资源鉴权
 *
 * @author zhoukui
 *
 */
@Component
public class AdminLoginFilter implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminLoginFilter.class);

    public static final String ID = "adminId";

    public static final String NAME = "userName";

    @Autowired
    private AdminJwtHelper jwtHelper;

    @Override
    public boolean preHandle(
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse,
            Object handler) throws Exception {
        if (RequestMethod.OPTIONS.toString().equals(httpRequest.getMethod())) {
            return true;
        }
        String jwt = getJWT(httpRequest);

        Response response = jwtHelper.parse(jwt);

        Admin admin = response.getAdmin();

        if (admin == null) {
            LOGGER.debug("admin = {}", admin);
            throw new NeedLoginError();
        }

        httpRequest.setAttribute(ID, admin.getAdminId());
        httpRequest.setAttribute(NAME, admin.getUserName());

        return true;
    }

    private String getJWT(
            HttpServletRequest httpRequest) {
        String jwt = httpRequest.getHeader("TOKEN");

        if (StringUtils.isBlank(jwt)) {
            jwt = httpRequest.getParameter("token");
        }

        return jwt;
    }

    @Override
    public void postHandle(
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse,
            Object handler,
            ModelAndView modelAndView) throws Exception {
        /*
         * not to do
         */
    }

    @Override
    public void afterCompletion(
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse,
            Object handler,
            Exception ex) throws Exception {
        /*
         * not to do
         */
    }

}