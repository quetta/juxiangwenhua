package com.juxiangwenhua.api.commons.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * json web token config
 *
 * @author zhoukui
 *
 */
@Component
@RefreshScope
@ConfigurationProperties(prefix = "jwt")
public class JwtConfig {

    /**
     * signingKey
     */
    private String signingKey = "signingKey";

    /**
     * audience
     */
    private String audience = "";

    /**
     * issuer
     */
    private String issuer = "";

    /**
     * TTLMillis
     */
    private long ttlMillis = 86400000;

    /**
     * 返回signingKey
     *
     * @return signingKey
     */
    public String getSigningKey() {
        return signingKey;
    }

    /**
     * 设置signingKey
     *
     * @param signingKey
     *            signingKey
     */
    public void setSigningKey(
            String signingKey) {
        this.signingKey = signingKey;
    }

    /**
     * 返回audience
     *
     * @return audience
     */
    public String getAudience() {
        return audience;
    }

    /**
     * 设置audience
     *
     * @param audience
     *            audience
     */
    public void setAudience(
            String audience) {
        this.audience = audience;
    }

    /**
     * 返回issuer
     *
     * @return issuer
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * 设置issuer
     *
     * @param issuer
     *            issuer
     */
    public void setIssuer(
            String issuer) {
        this.issuer = issuer;
    }

    /**
     * 返回 ttlMillis
     *
     * @return ttlMillis
     */
    public long getTtlMillis() {
        return ttlMillis;
    }

    /**
     * 设置ttlMillis
     *
     * @param ttlMillis
     *            ttlMillis
     */
    public void setTtlMillis(
            long ttlMillis) {
        this.ttlMillis = ttlMillis;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("JWTConfig [signingKey=");
        builder.append(signingKey);
        builder.append(", audience=");
        builder.append(audience);
        builder.append(", issuer=");
        builder.append(issuer);
        builder.append(", ttlMillis=");
        builder.append(ttlMillis);
        builder.append("]");
        return builder.toString();
    }
}
