package com.juxiangwenhua.api.commons.jwt;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.juxiangwenhua.api.model.admin.entity.Admin;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.guerlab.commons.exception.ApplicationException;

/**
 * json web token助手
 *
 * @author zhoukui
 *
 */
@Component
public class AdminJwtHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminJwtHelper.class);

    public static final String USERNAME = "userName";

    public static final String ID = "adminId";

    @Autowired
    private JwtConfig config;

    private byte[] createKey() {
        return DatatypeConverter.parseBase64Binary(config.getSigningKey() + "_AdminJWTHelper");
    }

    /**
     * 解析响应内容
     *
     * @author zhoukui
     *
     */
    public static class Response {

        private Admin admin;

        private long ttlMillis = -1;

        private Response() {

        }

        /**
         * 返回用户对象
         *
         * @return 用户对象
         */
        public Admin getAdmin() {
            return admin;
        }

        /**
         * 返回超时时间设置
         *
         * @return 超时时间设置
         */
        public long getTtlMillis() {
            return ttlMillis;
        }

    }

    /**
     * 解析用户
     *
     * @param jwt
     *            令牌
     * @return 响应内容
     */
    public Response parse(
            String jwt) {

        Response response = new Response();
        Claims claims = null;

        try {
            claims = Jwts.parser().setSigningKey(createKey()).parseClaimsJws(jwt).getBody();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return response;
        }

        if (claims == null) {
            return response;
        }

        Admin admin = new Admin();

        try {
            admin.setAdminId(Long.parseLong(claims.get(ID).toString()));
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            throw new ApplicationException(e);
        }

        admin.setUserName(String.valueOf(claims.get(USERNAME)));

        response.admin = admin;

        Date exp = claims.getExpiration();
        Date nbf = claims.getNotBefore();

        if (exp != null && nbf != null) {
            response.ttlMillis = exp.getTime() - nbf.getTime();
        }

        return response;
    }

    /**
     * 生成令牌
     *
     * @param admin
     *            管理员对象
     * @param ttlMillis
     *            超时时间
     * @return json web token
     */
    public String create(
            Admin admin,
            long ttlMillis) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        // 生成签名密钥
        Key signingKey = new SecretKeySpec(createKey(), signatureAlgorithm.getJcaName());

        // 添加构成JWT的参数
        JwtBuilder builder = Jwts.builder();
        builder.setHeaderParam("typ", "JWT");
        builder.claim(ID, admin.getAdminId());
        builder.claim(USERNAME, admin.getUserName());
        builder.setIssuer(config.getIssuer());
        builder.setAudience(config.getAudience());
        builder.signWith(signatureAlgorithm, signingKey);

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // 添加Token过期时间
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp).setNotBefore(now);
        } else if (config.getTtlMillis() >= 0) {
            long expMillis = nowMillis + config.getTtlMillis();
            Date exp = new Date(expMillis);
            builder.setExpiration(exp).setNotBefore(now);
        }

        // 生成JWT
        return builder.compact();
    }
}
