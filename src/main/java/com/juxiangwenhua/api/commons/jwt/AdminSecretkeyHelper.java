package com.juxiangwenhua.api.commons.jwt;

import java.text.NumberFormat;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import com.juxiangwenhua.api.model.admin.entity.Admin;

import net.guerlab.commons.random.RandomUtil;

/**
 * 用户密钥助手
 *
 * @author zhoukui
 *
 */
public class AdminSecretkeyHelper {

    private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();

    private static final int PASSWORD_LENGTH = 32;

    static {
        NUMBER_FORMAT.setGroupingUsed(false);
        NUMBER_FORMAT.setMaximumIntegerDigits(6);
        NUMBER_FORMAT.setMinimumIntegerDigits(6);
    }

    private AdminSecretkeyHelper() {
        throw new IllegalAccessError();
    }

    /**
     * 获取随机密码
     *
     * @return 随机密码
     */
    public static String getRandomSecretkey() {
        return NUMBER_FORMAT.format(RandomUtil.nextInt(1000000));
    }

    /**
     * 修改密码
     *
     * @param admin
     *            用户对象
     * @param secretkey
     *            新密码
     */
    public static final void modifySecretkey(
            Admin admin,
            String secretkey) {
        if (admin == null || StringUtils.isBlank(secretkey)) {
            return;
        }

        admin.setSalt(RandomUtil.nextString(6));

        admin.setSecretkey(DigestUtils.md5Hex(DigestUtils.md5Hex(secretkey) + admin.getSalt()));
    }

    /**
     * 用户密码检查
     *
     * @param admin
     *            用户对象
     * @param secretkey
     *            密码
     * @return 密码是否正确
     */
    public static final boolean isSecretkeyValid(
            Admin admin,
            String secretkey) {
        if (admin == null || StringUtils.isBlank(secretkey)) {
            return false;
        }

        return isSecretkeyValid(admin.getSecretkey(), secretkey, admin.getSalt());
    }

    /**
     * 判断密码是否正确
     *
     * @param savePass
     *            保存的密码
     * @param submitPass
     *            提交的密码
     * @param salt
     *            安全码
     * @return 密码是否正确
     */
    public static final boolean isSecretkeyValid(
            String savePass,
            String submitPass,
            String salt) {
        if (savePass == null || savePass.length() != PASSWORD_LENGTH || StringUtils.isBlank(submitPass)
                || salt == null) {
            return false;
        }

        return savePass.equals(DigestUtils.md5Hex(DigestUtils.md5Hex(submitPass) + salt));
    }
}