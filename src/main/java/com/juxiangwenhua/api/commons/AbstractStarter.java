package com.juxiangwenhua.api.commons;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import net.guerlab.spring.commons.util.SpringApplicationContextUtil;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;

public abstract class AbstractStarter extends WebMvcConfigurerAdapter implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractStarter.class);

    protected List<Parameter> aParameters = Arrays.asList(new ParameterBuilder().parameterType("header").name("TOKEN")
            .defaultValue("").description("身份令牌").modelRef(new ModelRef("string")).required(false).build());

    @Override
    public void addResourceHandlers(
            ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void run(
            String... args) throws Exception {
        LOGGER.info("start server[port={}]",
                SpringApplicationContextUtil.getContext().getEnvironment().getProperty("server.port"));
    }

    protected ApiInfo apiInfo() {
        ApiInfoBuilder builder = new ApiInfoBuilder();

        builder.title(getServiceTitle());
        builder.description(getServiceDescription());
        builder.version(getServiceVersion());

        return builder.build();
    }

    protected Docket create(
            String groupName,
            String basePackage) {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);

        docket.apiInfo(apiInfo());
        docket.groupName(groupName);
        docket.useDefaultResponseMessages(false);

        ApiSelectorBuilder selectorBuilder = docket.select();

        selectorBuilder.apis(RequestHandlerSelectors.basePackage(basePackage));
        selectorBuilder.paths(PathSelectors.any());
        selectorBuilder.build();

        return docket;
    }

    protected String getServiceTitle() {
        return null;
    };

    protected String getServiceDescription() {
        return null;
    };

    protected String getServiceVersion() {
        return "1.0";
    }
}
