package com.juxiangwenhua.api.model.admin.service;

import java.time.LocalDateTime;

import com.juxiangwenhua.api.base.dto.LoginDTO;
import com.juxiangwenhua.api.base.exception.LoginFailError;
import com.juxiangwenhua.api.base.service.BaseService;
import com.juxiangwenhua.api.commons.jwt.AdminSecretkeyHelper;
import com.juxiangwenhua.api.model.admin.entity.Admin;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 管理员
 *
 * @author kuisama
 *
 */
public interface AdminService extends BaseService<Admin> {

    default Admin login(
            LoginDTO dto) {
        Admin admin = new Admin();

        admin.setUserName(dto.getUserName());
        Admin user = selectOne(admin);

        if (user == null) {
            throw new LoginFailError("帐号不存在", 801);
        }

        if (!AdminSecretkeyHelper.isSecretkeyValid(user, dto.getSecretkey())) {
            throw new LoginFailError("密码错误", 802);
        }

        user.setLastLoginTime(LocalDateTime.now());
        updateSelectiveById(user);

        return user;

    }

    /**
     * 修改密码
     *
     * @param user
     *            用户
     * @param secretkey
     *            密码
     */
    default void updateSecretkey(
            Admin admin,
            String secretkey) {
        if (admin == null) {
            throw new ApplicationException("管理员无效");
        }
        AdminSecretkeyHelper.modifySecretkey(admin, secretkey);

        updateSelectiveById(admin);
    }

    /**
     * 
     * @param user
     * @param secretkey
     */
    default void save(
            Admin admin,
            String secretkey) {
        if (admin == null) {
            throw new ApplicationException("管理员无效");
        }

        AdminSecretkeyHelper.modifySecretkey(admin, secretkey);

        insertSelective(admin);
    }
}
