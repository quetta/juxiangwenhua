package com.juxiangwenhua.api.model.admin.service.impl;

import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.exception.UserNameRepeatException;
import com.juxiangwenhua.api.base.exception.AdminInvalidException;
import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.admin.entity.Admin;
import com.juxiangwenhua.api.model.admin.mapper.AdminMapper;
import com.juxiangwenhua.api.model.admin.service.AdminService;

import net.guerlab.spring.commons.sequence.Sequence;

@Service
public class AdminServiceImpl extends BaseServiceImpl<AdminMapper, Admin> implements AdminService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            Admin admin) {
        if (admin == null) {
            throw new AdminInvalidException();
        }

        String userName = StringUtils.trim(admin.getUserName());

        if (StringUtils.isBlank(userName)) {
            throw new AdminInvalidException();
        }

        userNameRepeatCheck(userName);

        admin.setAdminId(sequence.nextId());
        admin.setRegistrationTime(LocalDateTime.now());
        admin.setLastLoginTime(LocalDateTime.now());
    }

    private void userNameRepeatCheck(
            String username) {
        Admin entity = new Admin();
        entity.setUserName(username);

        if (selectOne(entity) != null) {
            throw new UserNameRepeatException();
        }
    }

}
