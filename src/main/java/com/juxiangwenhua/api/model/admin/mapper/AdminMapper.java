package com.juxiangwenhua.api.model.admin.mapper;

import com.juxiangwenhua.api.model.admin.entity.Admin;

import tk.mybatis.mapper.common.Mapper;

public interface AdminMapper extends Mapper<Admin> {

}
