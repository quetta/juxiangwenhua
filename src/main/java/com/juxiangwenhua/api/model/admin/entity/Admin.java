package com.juxiangwenhua.api.model.admin.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 管理员
 *
 *
 */
@Table(name = "admin_admin")
@ApiModel("管理员")
public class Admin {

    /**
     * 管理员id
     */
    @Id
    @ApiModelProperty("管理员id")
    protected Long adminId;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    protected String userName;

    /**
     * 密钥
     */
    @JsonIgnore
    private String secretkey;

    /**
     * 随机码
     */
    @JsonIgnore
    private String salt;

    public Admin() {
    }

    public Admin(Long adminId, String userName) {
        this.adminId = adminId;
        this.userName = userName;
    }

    /**
     * 注册时间
     */
    @ApiModelProperty("注册时间")
    private LocalDateTime registrationTime;

    /**
     * 上次登录时间
     */
    @ApiModelProperty("上次登陆时间")
    private LocalDateTime lastLoginTime;

    /**
     * 返回adminId
     *
     * @return adminId
     */
    public Long getAdminId() {
        return adminId;
    }

    /**
     * 设置adminId
     *
     * @param adminId
     *            adminId
     */
    public void setAdminId(
            Long adminId) {
        this.adminId = adminId;
    }

    /**
     * 返回userName
     *
     * @return userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置userName
     *
     * @param userName
     *            userName
     */
    public void setUserName(
            String userName) {
        this.userName = userName;
    }

    /**
     * 返回secretkey
     *
     * @return secretkey
     */
    public String getSecretkey() {
        return secretkey;
    }

    /**
     * 设置secretkey
     *
     * @param secretkey
     *            secretkey
     */
    public void setSecretkey(
            String secretkey) {
        this.secretkey = secretkey;
    }

    /**
     * 返回salt
     *
     * @return salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 设置salt
     *
     * @param salt
     *            salt
     */
    public void setSalt(
            String salt) {
        this.salt = salt;
    }

    /**
     * 返回registrationTime
     *
     * @return registrationTime
     */
    public LocalDateTime getRegistrationTime() {
        return registrationTime;
    }

    /**
     * 设置registrationTime
     *
     * @param registrationTime
     *            registrationTime
     */
    public void setRegistrationTime(
            LocalDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }

    /**
     * 返回lastLoginTime
     *
     * @return lastLoginTime
     */
    public LocalDateTime getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 设置lastLoginTime
     *
     * @param lastLoginTime
     *            lastLoginTime
     */
    public void setLastLoginTime(
            LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

}
