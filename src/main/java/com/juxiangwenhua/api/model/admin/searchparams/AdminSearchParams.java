package com.juxiangwenhua.api.model.admin.searchparams;

import javax.persistence.Column;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 * 管理员搜索参数
 *
 *
 */
@ApiModel("管理员搜索参数")
public class AdminSearchParams extends AbstractSearchParams {

    /**
     * 管理员ID
     */
    @ApiModelProperty("管理员ID")
    private Long adminId;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    @SearchModel(SearchModelType.LIKE)
    private String userName;

    @ApiModelProperty("排序")
    @Column(name = "registrationTime")
    private OrderByType orderBy;

    /**
     * 返回adminId
     *
     * @return adminId
     */
    public Long getAdminId() {
        return adminId;
    }

    /**
     * 设置adminId
     *
     * @param adminId
     *            adminId
     */
    public void setAdminId(
            Long adminId) {
        this.adminId = adminId;
    }

    /**
     * 返回userName
     *
     * @return userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置userName
     *
     * @param userName
     *            userName
     */
    public void setUserName(
            String userName) {
        this.userName = userName;
    }

    /**
     * 返回orderBy
     * 
     * @return orderBy
     */
    public OrderByType getOrderBy() {
        return orderBy;
    }

    /**
     * 设置orderBy
     * 
     * @param orderBy
     *            orderBy
     */
    public void setOrderBy(
            OrderByType orderBy) {
        this.orderBy = orderBy;
    }

}
