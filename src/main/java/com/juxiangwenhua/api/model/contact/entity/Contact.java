package com.juxiangwenhua.api.model.contact.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author kuisama
 *
 */
@ApiModel("联系我们")
@Table(name = "contact_contact")
public class Contact {

    @Id
    @ApiModelProperty("ID")
    private Long contactId;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("phone")
    private String phone;

    @ApiModelProperty("email")
    private String email;

    @ApiModelProperty("company")
    private String company;

    @ApiModelProperty("message")
    private String message;

    @ApiModelProperty("addTime")
    private LocalDateTime addTime;

    /**
     * 返回contactId
     * 
     * @return contactId
     */
    public Long getContactId() {
        return contactId;
    }

    /**
     * 设置contactId
     * 
     * @param contactId
     *            contactId
     */
    public void setContactId(
            Long contactId) {
        this.contactId = contactId;
    }

    /**
     * 返回name
     * 
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置name
     * 
     * @param name
     *            name
     */
    public void setName(
            String name) {
        this.name = name;
    }

    /**
     * 返回phone
     * 
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置phone
     * 
     * @param phone
     *            phone
     */
    public void setPhone(
            String phone) {
        this.phone = phone;
    }

    /**
     * 返回email
     * 
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置email
     * 
     * @param email
     *            email
     */
    public void setEmail(
            String email) {
        this.email = email;
    }

    /**
     * 返回company
     * 
     * @return company
     */
    public String getCompany() {
        return company;
    }

    /**
     * 设置company
     * 
     * @param company
     *            company
     */
    public void setCompany(
            String company) {
        this.company = company;
    }

    /**
     * 返回message
     * 
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 设置message
     * 
     * @param message
     *            message
     */
    public void setMessage(
            String message) {
        this.message = message;
    }

    /**
     * 返回addTime
     * 
     * @return addTime
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 设置addTime
     * 
     * @param addTime
     *            addTime
     */
    public void setAddTime(
            LocalDateTime addTime) {
        this.addTime = addTime;
    }

}
