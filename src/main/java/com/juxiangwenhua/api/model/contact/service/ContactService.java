package com.juxiangwenhua.api.model.contact.service;

import com.juxiangwenhua.api.base.service.BaseService;
import com.juxiangwenhua.api.model.contact.entity.Contact;

/**
 * 
 * @author kuisama
 *
 */
public interface ContactService extends BaseService<Contact> {

}
