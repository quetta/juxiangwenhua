package com.juxiangwenhua.api.model.contact.searchparams;

import javax.persistence.Column;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 * 
 * @author kuisama
 *
 */
@ApiModel("搜索参数")
public class ContactSearchParams extends AbstractSearchParams {

    @ApiModelProperty("ID")
    private Long contactId;

    @ApiModelProperty("name")
    @SearchModel(SearchModelType.LIKE)
    private String name;

    @ApiModelProperty("phone")
    @SearchModel(SearchModelType.LIKE)
    private String phone;

    @ApiModelProperty("email")
    @SearchModel(SearchModelType.LIKE)
    private String email;

    @ApiModelProperty("company")
    @SearchModel(SearchModelType.LIKE)
    private String company;

    @ApiModelProperty("排序")
    @Column(name = "addTime")
    private OrderByType orderBy;

    /**
     * 返回contactId
     * 
     * @return contactId
     */
    public Long getContactId() {
        return contactId;
    }

    /**
     * 设置contactId
     * 
     * @param contactId
     *            contactId
     */
    public void setContactId(
            Long contactId) {
        this.contactId = contactId;
    }

    /**
     * 返回name
     * 
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置name
     * 
     * @param name
     *            name
     */
    public void setName(
            String name) {
        this.name = name;
    }

    /**
     * 返回phone
     * 
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置phone
     * 
     * @param phone
     *            phone
     */
    public void setPhone(
            String phone) {
        this.phone = phone;
    }

    /**
     * 返回email
     * 
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置email
     * 
     * @param email
     *            email
     */
    public void setEmail(
            String email) {
        this.email = email;
    }

    /**
     * 返回company
     * 
     * @return company
     */
    public String getCompany() {
        return company;
    }

    /**
     * 设置company
     * 
     * @param company
     *            company
     */
    public void setCompany(
            String company) {
        this.company = company;
    }

    /**
     * 返回orderBy
     * 
     * @return orderBy
     */
    public OrderByType getOrderBy() {
        return orderBy;
    }

    /**
     * 设置orderBy
     * 
     * @param orderBy
     *            orderBy
     */
    public void setOrderBy(
            OrderByType orderBy) {
        this.orderBy = orderBy;
    }

}
