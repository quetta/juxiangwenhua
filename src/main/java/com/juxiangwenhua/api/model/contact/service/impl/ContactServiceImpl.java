package com.juxiangwenhua.api.model.contact.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.contact.entity.Contact;
import com.juxiangwenhua.api.model.contact.mapper.ContactMapper;
import com.juxiangwenhua.api.model.contact.service.ContactService;

import net.guerlab.spring.commons.sequence.Sequence;

/**
 * 
 * @author kuisama
 *
 */
@Service
public class ContactServiceImpl extends BaseServiceImpl<ContactMapper, Contact> implements ContactService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            Contact entity) {
        entity.setContactId(sequence.nextId());
        entity.setAddTime(LocalDateTime.now());
    }

}
