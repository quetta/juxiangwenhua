package com.juxiangwenhua.api.model.contact.mapper;

import com.juxiangwenhua.api.model.contact.entity.Contact;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * @author kuisama
 *
 */
public interface ContactMapper extends Mapper<Contact> {

}
