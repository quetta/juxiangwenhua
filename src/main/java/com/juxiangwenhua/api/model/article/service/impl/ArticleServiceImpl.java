package com.juxiangwenhua.api.model.article.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.article.entity.Article;
import com.juxiangwenhua.api.model.article.mapper.ArticleMapper;
import com.juxiangwenhua.api.model.article.service.ArticleService;

import net.guerlab.spring.commons.sequence.Sequence;

@Service
public class ArticleServiceImpl extends BaseServiceImpl<ArticleMapper, Article> implements ArticleService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            Article entity) {
        entity.setArticleId(sequence.nextId());
        entity.setAddTime(LocalDateTime.now());
    }

}
