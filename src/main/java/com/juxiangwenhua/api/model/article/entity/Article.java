package com.juxiangwenhua.api.model.article.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文章
 *
 * @author kuisama
 *
 */
@ApiModel("文章")
@Table(name = "article_article")
public class Article {

    @Id
    @ApiModelProperty("文章ID")
    private Long articleId;

    @ApiModelProperty("文章内容")
    private String articleContent;

    @ApiModelProperty("文章类型")
    private ArticleType articleType;

    @ApiModelProperty("跳转链接")
    private String jumpUrl;

    @ApiModelProperty("添加时间")
    private LocalDateTime addTime;

    /**
     * 返回 文章ID
     *
     * @return 文章ID
     */
    public final Long getArticleId() {
        return articleId;
    }

    /**
     * 设置文章ID
     *
     * @param articleId
     *            文章ID
     */
    public final void setArticleId(
            Long articleId) {
        this.articleId = articleId;
    }

    /**
     * 返回 文章内容
     *
     * @return 文章内容
     */
    public final String getArticleContent() {
        return articleContent;
    }

    /**
     * 设置文章内容
     *
     * @param articleContent
     *            文章内容
     */
    public final void setArticleContent(
            String articleContent) {
        this.articleContent = articleContent;
    }

    /**
     * 返回articleType
     *
     * @return articleType
     */
    public ArticleType getArticleType() {
        return articleType;
    }

    /**
     * 设置articleType
     *
     * @param articleType
     *            articleType
     */
    public void setArticleType(
            ArticleType articleType) {
        this.articleType = articleType;
    }

    /**
     * 返回addTime
     *
     * @return addTime
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 设置addTime
     *
     * @param addTime
     *            addTime
     */
    public void setAddTime(
            LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 返回 跳转链接
     *
     * @return 跳转链接
     */
    public String getJumpUrl() {
        return jumpUrl;
    }

    /**
     * 设置跳转链接
     *
     * @param jumpUrl
     *            跳转链接
     */
    public void setJumpUrl(
            String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

}
