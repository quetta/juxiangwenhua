package com.juxiangwenhua.api.model.article.entity;

/**
 * 文章类型
 *
 * @author kuisama
 *
 */
public enum ArticleType {
    /**
     * 主页
     */
    INDEX,
    /**
     * 关于我们
     */
    ABOUTAS,
    /**
     * 联系我们
     */
    CONTACTAS;
}
