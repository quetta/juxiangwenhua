package com.juxiangwenhua.api.model.article.service;

import com.juxiangwenhua.api.base.service.BaseService;
import com.juxiangwenhua.api.model.article.entity.Article;

import net.guerlab.commons.number.NumberHelper;

/**
 * 文章服务
 *
 * @author zhoukui
 *
 */
public interface ArticleService extends BaseService<Article> {

    /**
     * 根据文章ID查询文章
     *
     * @param articleId
     *            文章ID
     * @return 文章
     */
    default Article findOne(
            Long articleId) {
        if (!NumberHelper.greaterZero(articleId)) {
            return null;
        }

        Article entity = new Article();
        entity.setArticleId(articleId);
        return selectOne(entity);
    }

}
