package com.juxiangwenhua.api.model.article.searchparams;

import javax.persistence.Column;

import com.juxiangwenhua.api.model.article.entity.ArticleType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 * 文章搜索参数
 *
 * @author zhoukui
 *
 */
@ApiModel("文章搜索参数")
public class ArticleSearchParams extends AbstractSearchParams {

    /**
     * 文章ID
     */
    @ApiModelProperty("文章ID")
    private Long articleId;

    /**
     * 文章标题
     */
    @ApiModelProperty("文章标题")
    @SearchModel(SearchModelType.LIKE)
    private String articleTitle;

    @ApiModelProperty("文章类型")
    private ArticleType articleType;

    @ApiModelProperty("排序")
    @Column(name = "addTime")
    private OrderByType orderBy;

    /**
     * 返回 文章ID
     *
     * @return 文章ID
     */
    public final Long getArticleId() {
        return articleId;
    }

    /**
     * 设置文章ID
     *
     * @param articleId
     *            文章ID
     */
    public final void setArticleId(
            Long articleId) {
        this.articleId = articleId;
    }

    /**
     * 返回articleTitle
     *
     * @return articleTitle
     */
    public String getArticleTitle() {
        return articleTitle;
    }

    /**
     * 设置articleTitle
     *
     * @param articleTitle
     *            articleTitle
     */
    public void setArticleTitle(
            String articleTitle) {
        this.articleTitle = articleTitle;
    }

    /**
     * 返回articleType
     *
     * @return articleType
     */
    public ArticleType getArticleType() {
        return articleType;
    }

    /**
     * 设置articleType
     *
     * @param articleType
     *            articleType
     */
    public void setArticleType(
            ArticleType articleType) {
        this.articleType = articleType;
    }

    /**
     * 返回orderBy
     * 
     * @return orderBy
     */
    public OrderByType getOrderBy() {
        return orderBy;
    }

    /**
     * 设置orderBy
     * 
     * @param orderBy
     *            orderBy
     */
    public void setOrderBy(
            OrderByType orderBy) {
        this.orderBy = orderBy;
    }

}
