package com.juxiangwenhua.api.model.article.mapper;

import com.juxiangwenhua.api.model.article.entity.Article;

import tk.mybatis.mapper.common.Mapper;

/**
 *
 * @author kuisama
 *
 */
public interface ArticleMapper extends Mapper<Article> {

}
