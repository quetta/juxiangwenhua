package com.juxiangwenhua.api.model.vedio.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 视频
 *
 * @author kuisama
 *
 */
@ApiModel("视频")
@Table(name = "vedio_vedio")
public class Vedio {

    @Id
    @ApiModelProperty("视频ID")
    private Long vedioId;

    @ApiModelProperty("视频名称")
    private String vedioName;

    @ApiModelProperty("视频类型")
    private VedioType vedioType;

    @ApiModelProperty("视频URL")
    private String vedioURL;

    @ApiModelProperty("添加时间")
    private LocalDateTime addTime;

    @ApiModelProperty("时长")
    private String duration;

    @ApiModelProperty("描述")
    private String vedioDesc;

    /**
     * 返回视频ID
     *
     * @return vedioId
     */
    public Long getVedioId() {
        return vedioId;
    }

    /**
     * 设置视频ID
     *
     * @param vedioId
     *            视频ID
     */
    public void setVedioId(
            Long vedioId) {
        this.vedioId = vedioId;
    }

    /**
     * 返回视频名称
     *
     * @return vedioName
     */
    public String getVedioName() {
        return vedioName;
    }

    /**
     * 设置视频名称
     *
     * @param vedioName
     *            视频名称
     */
    public void setVedioName(
            String vedioName) {
        this.vedioName = vedioName;
    }

    /**
     * 返回视频类型
     *
     * @return vedioType
     */
    public VedioType getVedioType() {
        return vedioType;
    }

    /**
     * 设置视频类型
     *
     * @param vedioType
     *            视频类型
     */
    public void setVedioType(
            VedioType vedioType) {
        this.vedioType = vedioType;
    }

    /**
     * 返回视频URL
     *
     * @return vedioURL
     */
    public String getVedioURL() {
        return vedioURL;
    }

    /**
     * 设置视频URL
     *
     * @param vedioURL
     *            视频URL
     */
    public void setVedioURL(
            String vedioURL) {
        this.vedioURL = vedioURL;
    }

    /**
     * 返回添加时间
     *
     * @return addTime
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime
     *            添加时间
     */
    public void setAddTime(
            LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 返回时长
     *
     * @return duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * 设置时长
     *
     * @param duration
     *            时长
     */
    public void setDuration(
            String duration) {
        this.duration = duration;
    }

    /**
     * 返回描述
     *
     * @return vedioDesc
     */
    public String getVedioDesc() {
        return vedioDesc;
    }

    /**
     * 设置描述
     *
     * @param vedioDesc
     *            描述
     */
    public void setVedioDesc(
            String vedioDesc) {
        this.vedioDesc = vedioDesc;
    }

}
