package com.juxiangwenhua.api.model.vedio.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.vedio.entity.Vedio;
import com.juxiangwenhua.api.model.vedio.mapper.VedioMapper;
import com.juxiangwenhua.api.model.vedio.service.VedioService;

import net.guerlab.spring.commons.sequence.Sequence;

/**
 *
 * @author kuisama
 *
 */
@Service
public class VedioServiceImpl extends BaseServiceImpl<VedioMapper, Vedio> implements VedioService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            Vedio entity) {
        entity.setVedioId(sequence.nextId());
        entity.setAddTime(LocalDateTime.now());
    }

}
