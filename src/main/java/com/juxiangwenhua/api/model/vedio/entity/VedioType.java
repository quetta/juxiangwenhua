package com.juxiangwenhua.api.model.vedio.entity;

/**
 * 视频类型
 *
 * @author quetta
 *
 */
public enum VedioType {
    /**
     * 首页视频
     */
    INDEX_VEDIO,
    /**
     * 广告
     */
    AD,
    /**
     * 宣传片
     */
    TRAILER,
    /**
     * 短片
     */
    SHORT_FILM;
}
