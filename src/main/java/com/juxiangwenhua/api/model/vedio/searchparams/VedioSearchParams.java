package com.juxiangwenhua.api.model.vedio.searchparams;

import javax.persistence.Column;

import com.juxiangwenhua.api.model.vedio.entity.VedioType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 *
 * @author kuisama
 *
 */
@ApiModel("视频搜索参数")
public class VedioSearchParams extends AbstractSearchParams {

    @ApiModelProperty("视频ID")
    private Long vedioId;

    @ApiModelProperty("视频名称")
    @SearchModel(SearchModelType.LIKE)
    private String vedioName;

    @ApiModelProperty("视频类型")
    private VedioType vedioType;

    @ApiModelProperty("排序")
    @Column(name = "addTime")
    private OrderByType orderBy;

    /**
     * 返回vedioId
     *
     * @return vedioId
     */
    public Long getVedioId() {
        return vedioId;
    }

    /**
     * 设置vedioId
     *
     * @param vedioId
     *            vedioId
     */
    public void setVedioId(
            Long vedioId) {
        this.vedioId = vedioId;
    }

    /**
     * 返回vedioName
     *
     * @return vedioName
     */
    public String getVedioName() {
        return vedioName;
    }

    /**
     * 设置vedioName
     *
     * @param vedioName
     *            vedioName
     */
    public void setVedioName(
            String vedioName) {
        this.vedioName = vedioName;
    }

    /**
     * 返回vedioType
     *
     * @return vedioType
     */
    public VedioType getVedioType() {
        return vedioType;
    }

    /**
     * 设置vedioType
     *
     * @param vedioType
     *            vedioType
     */
    public void setVedioType(
            VedioType vedioType) {
        this.vedioType = vedioType;
    }

    /**
     * 返回orderBy
     * 
     * @return orderBy
     */
    public OrderByType getOrderBy() {
        return orderBy;
    }

    /**
     * 设置orderBy
     * 
     * @param orderBy
     *            orderBy
     */
    public void setOrderBy(
            OrderByType orderBy) {
        this.orderBy = orderBy;
    }

}
