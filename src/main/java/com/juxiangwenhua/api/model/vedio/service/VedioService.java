package com.juxiangwenhua.api.model.vedio.service;

import com.juxiangwenhua.api.base.service.BaseService;
import com.juxiangwenhua.api.model.vedio.entity.Vedio;

/**
 *
 * @author kuisama
 *
 */
public interface VedioService extends BaseService<Vedio> {

}
