package com.juxiangwenhua.api.model.vedio.mapper;

import com.juxiangwenhua.api.model.vedio.entity.Vedio;

import tk.mybatis.mapper.common.Mapper;

/**
 *
 * @author kuisama
 *
 */
public interface VedioMapper extends Mapper<Vedio> {

}
