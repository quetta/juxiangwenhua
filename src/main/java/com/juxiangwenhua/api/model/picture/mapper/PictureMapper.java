package com.juxiangwenhua.api.model.picture.mapper;

import com.juxiangwenhua.api.model.picture.entity.Picture;

import tk.mybatis.mapper.common.Mapper;

/**
 *
 * @author kuisama
 *
 */
public interface PictureMapper extends Mapper<Picture> {

}
