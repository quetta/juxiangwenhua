package com.juxiangwenhua.api.model.picture.searchparams;

import javax.persistence.Column;

import com.juxiangwenhua.api.model.picture.entity.PictureType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 *
 * @author kuisama
 *
 */
@ApiModel("图片搜索参数")
public class PictureSearchParams extends AbstractSearchParams {

    @ApiModelProperty("图片ID")
    private Long pictureId;

    @ApiModelProperty("图片名称")
    @SearchModel(SearchModelType.LIKE)
    private String pictureName;

    @ApiModelProperty("图片类型")
    private PictureType pictureType;

    @ApiModelProperty("排序")
    @Column(name = "addTime")
    private OrderByType orderBy;

    /**
     * 返回pictureId
     *
     * @return pictureId
     */
    public long getPictureId() {
        return pictureId;
    }

    /**
     * 设置pictureId
     *
     * @param pictureId
     *            pictureId
     */
    public void setPictureId(
            long pictureId) {
        this.pictureId = pictureId;
    }

    /**
     * 返回pictureName
     *
     * @return pictureName
     */
    public String getPictureName() {
        return pictureName;
    }

    /**
     * 设置pictureName
     *
     * @param pictureName
     *            pictureName
     */
    public void setPictureName(
            String pictureName) {
        this.pictureName = pictureName;
    }

    /**
     * 返回pictureType
     *
     * @return pictureType
     */
    public PictureType getPictureType() {
        return pictureType;
    }

    /**
     * 设置pictureType
     *
     * @param pictureType
     *            pictureType
     */
    public void setPictureType(
            PictureType pictureType) {
        this.pictureType = pictureType;
    }

    /**
     * 返回orderBy
     * 
     * @return orderBy
     */
    public OrderByType getOrderBy() {
        return orderBy;
    }

    /**
     * 设置orderBy
     * 
     * @param orderBy
     *            orderBy
     */
    public void setOrderBy(
            OrderByType orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * 设置pictureId
     * 
     * @param pictureId
     *            pictureId
     */
    public void setPictureId(
            Long pictureId) {
        this.pictureId = pictureId;
    }

}
