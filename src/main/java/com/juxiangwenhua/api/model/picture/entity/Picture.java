package com.juxiangwenhua.api.model.picture.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 图片
 *
 * @author kuisama
 *
 */
@ApiModel("图片")
@Table(name = "picture_picture")
public class Picture {

    @Id
    @ApiModelProperty("图片ID")
    private Long pictureId;

    @ApiModelProperty("图片名称")
    private String pictureName;

    @ApiModelProperty("图片类型")
    private PictureType pictureType;

    @ApiModelProperty("图片链接")
    private String pictureURL;

    @ApiModelProperty("跳转链接")
    private String jumpURL;

    @ApiModelProperty("添加时间")
    private LocalDateTime addTime;

    @ApiModelProperty("图片描述")
    private String pictureDesc;

    /**
     * 返回图片名称
     *
     * @return pictureName
     */
    public String getPictureName() {
        return pictureName;
    }

    /**
     * 设置图片名称
     *
     * @param pictureName
     *            图片名称
     */
    public void setPictureName(
            String pictureName) {
        this.pictureName = pictureName;
    }

    /**
     * 返回图片类型
     *
     * @return pictureType
     */
    public PictureType getPictureType() {
        return pictureType;
    }

    /**
     * 设置图片类型
     *
     * @param pictureType
     *            图片类型
     */
    public void setPictureType(
            PictureType pictureType) {
        this.pictureType = pictureType;
    }

    /**
     * 返回图片链接
     *
     * @return pictureURL
     */
    public String getPictureURL() {
        return pictureURL;
    }

    /**
     * 设置图片链接
     *
     * @param pictureURL
     *            图片链接
     */
    public void setPictureURL(
            String pictureURL) {
        this.pictureURL = pictureURL;
    }

    /**
     * 返回jumpURL
     *
     * @return jumpURL
     */
    public String getJumpURL() {
        return jumpURL;
    }

    /**
     * 设置jumpURL
     *
     * @param jumpURL
     *            jumpURL
     */
    public void setJumpURL(
            String jumpURL) {
        this.jumpURL = jumpURL;
    }

    /**
     * 设置pictureId
     *
     * @param pictureId
     *            pictureId
     */
    public void setPictureId(
            Long pictureId) {
        this.pictureId = pictureId;
    }

    /**
     * 返回pictureId
     *
     * @return pictureId
     */
    public Long getPictureId() {
        return pictureId;
    }

    /**
     * 返回添加时间
     *
     * @return addTime
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime
     *            添加时间
     */
    public void setAddTime(
            LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 返回图片描述
     *
     * @return pictureDesc
     */
    public String getPictureDesc() {
        return pictureDesc;
    }

    /**
     * 设置图片描述
     *
     * @param pictureDesc
     *            图片描述
     */
    public void setPictureDesc(
            String pictureDesc) {
        this.pictureDesc = pictureDesc;
    }

}
