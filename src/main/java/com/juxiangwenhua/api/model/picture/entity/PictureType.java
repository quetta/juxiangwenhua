package com.juxiangwenhua.api.model.picture.entity;

/**
 *
 * @author kuisama
 *
 */

public enum PictureType {
    /**
     * 服务分类图片
     */
    CATE_PIC_LIST,
    /**
     * 移动端背景
     */
    MOBILE_BACKGROUD_PICTURE,
    /**
     * PC端背景
     */
    PC_BACKGROUD_PICTURE,
    /**
     * 《关于我们》海报
     */
    ABOUT_AS_POSTERS,
    /**
     * 《视频》海报
     */
    VEDIO_POSTERS;
}
