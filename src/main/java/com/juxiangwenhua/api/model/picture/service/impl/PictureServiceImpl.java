package com.juxiangwenhua.api.model.picture.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.picture.entity.Picture;
import com.juxiangwenhua.api.model.picture.mapper.PictureMapper;
import com.juxiangwenhua.api.model.picture.service.PictureService;

import net.guerlab.spring.commons.sequence.Sequence;

/**
 *
 * @author kuisama
 *
 */
@Service
public class PictureServiceImpl extends BaseServiceImpl<PictureMapper, Picture> implements PictureService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            Picture entity) {
        entity.setPictureId(sequence.nextId());
        entity.setAddTime(LocalDateTime.now());
    }

}
