package com.juxiangwenhua.api.model.picture.service;

import com.juxiangwenhua.api.base.service.BaseService;
import com.juxiangwenhua.api.model.picture.entity.Picture;

/**
 *
 * @author kuisama
 *
 */
public interface PictureService extends BaseService<Picture> {

}
