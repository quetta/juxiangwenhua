package com.juxiangwenhua.api.model.ser.service;

import com.juxiangwenhua.api.base.service.BaseService;
import com.juxiangwenhua.api.model.ser.entity.SecCategory;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
public interface SecCategoryService extends BaseService<SecCategory> {

}
