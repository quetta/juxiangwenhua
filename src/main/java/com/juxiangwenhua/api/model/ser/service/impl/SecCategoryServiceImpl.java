package com.juxiangwenhua.api.model.ser.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.ser.entity.SecCategory;
import com.juxiangwenhua.api.model.ser.mapper.SecCategoryMapper;
import com.juxiangwenhua.api.model.ser.service.SecCategoryService;

import net.guerlab.spring.commons.sequence.Sequence;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@Service
public class SecCategoryServiceImpl
        extends BaseServiceImpl<SecCategoryMapper, SecCategory> implements SecCategoryService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            SecCategory entity) {
        entity.setSecCategoryId(sequence.nextId());
    }

}
