package com.juxiangwenhua.api.model.ser.mapper;

import com.juxiangwenhua.api.model.ser.entity.TopCategory;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
public interface TopCategoryMapper extends Mapper<TopCategory> {

}
