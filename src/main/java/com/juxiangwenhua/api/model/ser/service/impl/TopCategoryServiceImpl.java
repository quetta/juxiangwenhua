package com.juxiangwenhua.api.model.ser.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.ser.entity.TopCategory;
import com.juxiangwenhua.api.model.ser.mapper.TopCategoryMapper;
import com.juxiangwenhua.api.model.ser.service.TopCategoryService;

import net.guerlab.spring.commons.sequence.Sequence;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@Service
public class TopCategoryServiceImpl
        extends BaseServiceImpl<TopCategoryMapper, TopCategory> implements TopCategoryService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            TopCategory entity) {
        entity.setTopCategoryId(sequence.nextId());
    }

}
