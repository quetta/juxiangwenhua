package com.juxiangwenhua.api.model.ser.entity;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务介绍
 *
 * @author kuisama
 *
 */
@ApiModel("服务介绍")
@Table(name = "service_introduce")
public class ServiceIntroduce {

    @Id
    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("一级分类id")
    private Long topCategoryId;

    @ApiModelProperty("二级分类id")
    private Long secCategoryId;

    @ApiModelProperty("添加时间")
    private LocalDateTime addTime;

    /**
     * 返回id
     * 
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置id
     * 
     * @param id
     *            id
     */
    public void setId(
            Long id) {
        this.id = id;
    }

    /**
     * 返回内容
     * 
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     * 
     * @param content
     *            内容
     */
    public void setContent(
            String content) {
        this.content = content;
    }

    /**
     * 返回一级分类id
     * 
     * @return topCategoryId
     */
    public Long getTopCategoryId() {
        return topCategoryId;
    }

    /**
     * 设置一级分类id
     * 
     * @param topCategoryId
     *            一级分类id
     */
    public void setTopCategoryId(
            Long topCategoryId) {
        this.topCategoryId = topCategoryId;
    }

    /**
     * 返回二级分类id
     * 
     * @return secCategoryId
     */
    public Long getSecCategoryId() {
        return secCategoryId;
    }

    /**
     * 设置二级分类id
     * 
     * @param secCategoryId
     *            二级分类id
     */
    public void setSecCategoryId(
            Long secCategoryId) {
        this.secCategoryId = secCategoryId;
    }

    /**
     * 返回addTime
     * 
     * @return addTime
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 设置addTime
     * 
     * @param addTime
     *            addTime
     */
    public void setAddTime(
            LocalDateTime addTime) {
        this.addTime = addTime;
    }

}
