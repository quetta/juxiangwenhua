package com.juxiangwenhua.api.model.ser.searchparams;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@ApiModel("查询参数")
public class SecCategorySearchParams extends AbstractSearchParams {

    @ApiModelProperty("一级分类id")
    private Long topCategoryId;

    /**
     * 返回topCategoryId
     * 
     * @return topCategoryId
     */
    public Long getTopCategoryId() {
        return topCategoryId;
    }

    /**
     * 设置topCategoryId
     * 
     * @param topCategoryId
     *            topCategoryId
     */
    public void setTopCategoryId(
            Long topCategoryId) {
        this.topCategoryId = topCategoryId;
    }

}
