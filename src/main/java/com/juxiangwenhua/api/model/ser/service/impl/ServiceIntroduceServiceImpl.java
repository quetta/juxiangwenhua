package com.juxiangwenhua.api.model.ser.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.base.service.impl.BaseServiceImpl;
import com.juxiangwenhua.api.model.ser.entity.ServiceIntroduce;
import com.juxiangwenhua.api.model.ser.mapper.ServiceIntroduceMapper;
import com.juxiangwenhua.api.model.ser.service.ServiceIntroduceService;

import net.guerlab.spring.commons.sequence.Sequence;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@Service
public class ServiceIntroduceServiceImpl
        extends BaseServiceImpl<ServiceIntroduceMapper, ServiceIntroduce> implements ServiceIntroduceService {

    @Autowired
    private Sequence sequence;

    @Override
    protected void insertBefore(
            ServiceIntroduce entity) {
        entity.setId(sequence.nextId());
        entity.setAddTime(LocalDateTime.now());
    }

}
