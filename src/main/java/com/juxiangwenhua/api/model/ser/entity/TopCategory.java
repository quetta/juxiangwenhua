package com.juxiangwenhua.api.model.ser.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 一级分类
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@ApiModel("一级分类")
@Table(name = "top_category")
public class TopCategory {

    @Id
    @ApiModelProperty("一级分类id")
    private Long topCategoryId;

    @ApiModelProperty("一级分类名称")
    private String topCategoryName;

    @ApiModelProperty("一级分类icon")
    private String iconURL;

    /**
     * 返回一级分类id
     * 
     * @return topCategoryId
     */
    public Long getTopCategoryId() {
        return topCategoryId;
    }

    /**
     * 设置一级分类id
     * 
     * @param topCategoryId
     *            一级分类id
     */
    public void setTopCategoryId(
            Long topCategoryId) {
        this.topCategoryId = topCategoryId;
    }

    /**
     * 返回一级分类名称
     * 
     * @return topCategoryName
     */
    public String getTopCategoryName() {
        return topCategoryName;
    }

    /**
     * 设置一级分类名称
     * 
     * @param topCategoryName
     *            一级分类名称
     */
    public void setTopCategoryName(
            String topCategoryName) {
        this.topCategoryName = topCategoryName;
    }

    /**
     * 返回一级分类icon
     * 
     * @return iconURL
     */
    public String getIconURL() {
        return iconURL;
    }

    /**
     * 设置一级分类icon
     * 
     * @param iconURL
     *            一级分类icon
     */
    public void setIconURL(
            String iconURL) {
        this.iconURL = iconURL;
    }

}
