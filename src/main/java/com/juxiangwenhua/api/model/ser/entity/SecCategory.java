package com.juxiangwenhua.api.model.ser.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 二级分类
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
@ApiModel("二级分类")
@Table(name = "sec_category")
public class SecCategory {

    @Id
    @ApiModelProperty("二级分类id")
    private Long secCategoryId;

    @ApiModelProperty("二级分类名称")
    private String secCategoryName;

    @ApiModelProperty("一级分类id")
    private Long topCategoryId;

    /**
     * 返回二级分类id
     * 
     * @return secCategoryId
     */
    public Long getSecCategoryId() {
        return secCategoryId;
    }

    /**
     * 设置二级分类id
     * 
     * @param secCategoryId
     *            二级分类id
     */
    public void setSecCategoryId(
            Long secCategoryId) {
        this.secCategoryId = secCategoryId;
    }

    /**
     * 返回二级分类名称
     * 
     * @return secCategoryName
     */
    public String getSecCategoryName() {
        return secCategoryName;
    }

    /**
     * 设置二级分类名称
     * 
     * @param secCategoryName
     *            二级分类名称
     */
    public void setSecCategoryName(
            String secCategoryName) {
        this.secCategoryName = secCategoryName;
    }

    /**
     * 返回一级分类id
     * 
     * @return topCategoryId
     */
    public Long getTopCategoryId() {
        return topCategoryId;
    }

    /**
     * 设置一级分类id
     * 
     * @param topCategoryId
     *            一级分类id
     */
    public void setTopCategoryId(
            Long topCategoryId) {
        this.topCategoryId = topCategoryId;
    }

}
