package com.juxiangwenhua.api.model.ser.mapper;

import com.juxiangwenhua.api.model.ser.entity.ServiceIntroduce;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
public interface ServiceIntroduceMapper extends Mapper<ServiceIntroduce> {

}
