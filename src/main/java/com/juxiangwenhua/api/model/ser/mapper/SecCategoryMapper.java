package com.juxiangwenhua.api.model.ser.mapper;

import com.juxiangwenhua.api.model.ser.entity.SecCategory;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * @author zhoukui
 * @Date 2018年5月23日
 */
public interface SecCategoryMapper extends Mapper<SecCategory> {

}
