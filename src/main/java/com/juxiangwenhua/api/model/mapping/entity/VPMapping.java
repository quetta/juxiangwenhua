package com.juxiangwenhua.api.model.mapping.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 视频与图片关联
 * 
 * @author kuisama
 *
 */
@ApiModel("视频与图片关联")
@Table(name = "picture_vedio")
public class VPMapping {

    @Id
    @ApiModelProperty("图片id")
    private Long pictureId;

    @Id
    @ApiModelProperty("视频id")
    private Long vedioId;

    /**
     * 返回图片id
     * 
     * @return pictureId
     */
    public Long getPictureId() {
        return pictureId;
    }

    /**
     * 设置图片id
     * 
     * @param pictureId
     *            图片id
     */
    public void setPictureId(
            Long pictureId) {
        this.pictureId = pictureId;
    }

    /**
     * 返回视频id
     * 
     * @return vedioId
     */
    public Long getVedioId() {
        return vedioId;
    }

    /**
     * 设置视频id
     * 
     * @param vedioId
     *            视频id
     */
    public void setVedioId(
            Long vedioId) {
        this.vedioId = vedioId;
    }

}
