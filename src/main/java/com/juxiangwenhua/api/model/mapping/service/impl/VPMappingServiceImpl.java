package com.juxiangwenhua.api.model.mapping.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.juxiangwenhua.api.model.mapping.entity.VPMapping;
import com.juxiangwenhua.api.model.mapping.mapper.VPMappingMapper;
import com.juxiangwenhua.api.model.mapping.searchparams.VPMappingSearchParams;
import com.juxiangwenhua.api.model.mapping.service.VPMappingService;

import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.commons.number.NumberHelper;
import net.guerlab.spring.searchparams.SearchParamsUtils;
import tk.mybatis.mapper.entity.Example;

/**
 * 
 * @author kuisama
 *
 */
@Service
public class VPMappingServiceImpl implements VPMappingService {

    @Autowired
    private VPMappingMapper mapper;

    @Override
    public List<VPMapping> findList(
            VPMappingSearchParams searchParams) {
        Example example = new Example(VPMapping.class);
        SearchParamsUtils.setCriteria(searchParams, example);
        return mapper.selectByExample(example);
    }

    @Override
    public long findCount(
            VPMappingSearchParams searchParams) {
        Example example = new Example(VPMapping.class);
        SearchParamsUtils.setCriteria(searchParams, example);
        return mapper.selectCountByExample(example);
    }

    @Override
    public void delete(
            VPMapping mapping) {
        if (mapping == null) {
            throw new ApplicationException("关系不存在！");
        }
        mapper.delete(mapping);
    }

    @Override
    public VPMapping findVedio(
            Long pictureId) {
        if (!NumberHelper.greaterZero(pictureId)) {
            throw new ApplicationException("图片id无效");
        }
        VPMapping record = new VPMapping();
        record.setPictureId(pictureId);
        return mapper.selectOne(record);
    }

    @Override
    public VPMapping save(
            VPMapping mapping) {
        mapper.save(Arrays.asList(mapping));
        return mapping;
    }

    @Override
    public VPMapping findOne(
            Long pictureId) {
        return mapper.selectByPrimaryKey(pictureId);
    }

}
