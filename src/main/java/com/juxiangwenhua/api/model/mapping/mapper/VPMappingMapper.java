package com.juxiangwenhua.api.model.mapping.mapper;

import java.util.List;

import com.juxiangwenhua.api.model.mapping.entity.VPMapping;

import tk.mybatis.mapper.common.Mapper;

public interface VPMappingMapper extends Mapper<VPMapping> {

    /**
     * 保存关系列表
     *
     * @param list
     *            关系列表
     */
    void save(
            List<VPMapping> list);
}
