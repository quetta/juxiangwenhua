package com.juxiangwenhua.api.model.mapping.service;

import java.util.Collections;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.juxiangwenhua.api.model.mapping.entity.VPMapping;
import com.juxiangwenhua.api.model.mapping.searchparams.VPMappingSearchParams;

import net.guerlab.commons.number.NumberHelper;

/**
 * 
 * @author kuisama
 *
 */
@Transactional(rollbackFor = Exception.class)
public interface VPMappingService {

    /**
     * 查询关系列表
     *
     * @param searchParams
     *            搜索参数
     * @return 关系列表
     */
    List<VPMapping> findList(
            VPMappingSearchParams searchParams);

    /**
     * 查询列表个数
     * 
     * @param searchParams
     *            搜索参数
     * @return 列表个数
     */
    long findCount(
            VPMappingSearchParams searchParams);

    /**
     * 查询对应视频
     *
     * @param pictureId
     *            图片id
     * @return 关系详情
     */
    VPMapping findVedio(
            Long pictureId);

    /**
     * 通过图片id查询关系列表
     *
     * @param pictureId
     *            图片id
     * @return 关系列表
     */
    default List<VPMapping> findByPictureId(
            Long pictureId) {
        if (!NumberHelper.greaterZero(pictureId)) {
            return Collections.emptyList();
        }

        VPMappingSearchParams searchParams = new VPMappingSearchParams();
        searchParams.setPictureId(pictureId);

        return findList(searchParams);
    }

    /**
     * 保存关系
     *
     * @param mapping
     *            关系
     * 
     * @return 保存成功的关系
     */
    VPMapping save(
            VPMapping mapping);

    /**
     * 删除关系
     *
     * @param mapping
     *            关系
     * @param sendMessage
     *            是否发送队列消息
     */
    void delete(
            VPMapping mapping);

    VPMapping findOne(
            Long pictureId);
}
