package com.juxiangwenhua.api.model.mapping.searchparams;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.guerlab.spring.searchparams.AbstractSearchParams;
import net.guerlab.spring.searchparams.SearchModel;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 * 搜索参数
 * 
 * @author kuisama
 *
 */
@ApiModel("搜索参数")
public class VPMappingSearchParams extends AbstractSearchParams {

    @ApiModelProperty("图片id")
    private Long pictureId;

    @ApiModelProperty("视频id")
    private Long vedioId;

    @ApiModelProperty("图片名称")

    private String pictureName;

    @ApiModelProperty("视频名称")
    @SearchModel(SearchModelType.LIKE)
    private String vedioName;

    /**
     * 返回pictureId
     * 
     * @return pictureId
     */
    public Long getPictureId() {
        return pictureId;
    }

    /**
     * 设置pictureId
     * 
     * @param pictureId
     *            pictureId
     */
    public void setPictureId(
            Long pictureId) {
        this.pictureId = pictureId;
    }

    /**
     * 返回vedioId
     * 
     * @return vedioId
     */
    public Long getVedioId() {
        return vedioId;
    }

    /**
     * 设置vedioId
     * 
     * @param vedioId
     *            vedioId
     */
    public void setVedioId(
            Long vedioId) {
        this.vedioId = vedioId;
    }

    /**
     * 返回pictureName
     * 
     * @return pictureName
     */
    public String getPictureName() {
        return pictureName;
    }

    /**
     * 设置pictureName
     * 
     * @param pictureName
     *            pictureName
     */
    public void setPictureName(
            String pictureName) {
        this.pictureName = pictureName;
    }

    /**
     * 返回vedioName
     * 
     * @return vedioName
     */
    public String getVedioName() {
        return vedioName;
    }

    /**
     * 设置vedioName
     * 
     * @param vedioName
     *            vedioName
     */
    public void setVedioName(
            String vedioName) {
        this.vedioName = vedioName;
    }

}
