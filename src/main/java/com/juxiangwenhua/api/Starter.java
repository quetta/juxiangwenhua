package com.juxiangwenhua.api;

import javax.servlet.MultipartConfigElement;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.juxiangwenhua.api.commons.AbstractStarter;

import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("com.juxiangwenhua.api.**.mapper")
@EnableSwagger2
@Configuration
public class Starter extends AbstractStarter {

    public static void main(
            String[] args) {
        SpringApplication.run(Starter.class, args);
    }

    @Bean("admin-docket")
    public Docket admin() {
        return create("管理后台", "com.juxiangwenhua.api.controller.admin").globalOperationParameters(aParameters);
    }

    @Bean("upload-docket")
    public Docket upload() {
        return create("通用上传", "com.juxiangwenhua.api.controller.upload").globalOperationParameters(aParameters);
    }

    @Bean("picture-docket")
    public Docket picture() {
        return create("图片", "com.juxiangwenhua.api.controller.picture");
    }

    @Bean("vedio-docket")
    public Docket vedio() {
        return create("视频", "com.juxiangwenhua.api.controller.vedio");
    }

    @Bean("article-docket")
    public Docket article() {
        return create("文章", "com.juxiangwenhua.api.controller.article");
    }

    @Bean("contact-docket")
    public Docket contact() {
        return create("联系我们", "com.juxiangwenhua.api.controller.contact");
    }

    @Bean("mapping-docket")
    public Docket mapping() {
        return create("图片视频关联", "com.juxiangwenhua.api.controller.mapping");
    }

    @Bean("service-docket")
    public Docket service() {
        return create("服务分类", "com.juxiangwenhua.api.controller.ser");
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        // 单个文件最大
        factory.setMaxFileSize("500MB"); // KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize("1000MB");
        return factory.createMultipartConfig();
    }

    @Override
    protected String getServiceTitle() {
        return "聚象文化-接口服务";
    }

}
